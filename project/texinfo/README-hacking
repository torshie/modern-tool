$Id: README-hacking 6329 2015-06-09 23:14:56Z karl $
This file describes the development environment for Texinfo.

  Copyright 2002, 2003, 2005, 2006, 2007, 2008, 2010, 2011, 2012, 2013,
  2014, 2015 Free Software Foundation, Inc.

  Copying and distribution of this file, with or without modification,
  are permitted in any medium without royalty provided the copyright
  notice and this notice are preserved.

The development sources for GNU Texinfo are available through anonymous
Subversion at Savannah:
  http://savannah.gnu.org/svn/?group=texinfo

This distribution uses whatever versions of Automake, Autoconf, and
Gettext are listed in NEWS; usually the latest official releases.  If
you are getting the sources from the development repository (or change
configure.ac), you'll need to have these tools installed to (re)build.
You'll also need help2man.  If you modify texindex/ti.twjr, you'll need
gawk >= 4.0.  All of these programs are available from
http://ftp.gnu.org/gnu.

After getting the development sources, and installing the tools above,
you can run
 ./autogen.sh
and then, for example,
 ./configure -C CFLAGS='-g -Wdeclaration-after-statement'
and then
 make

After the initial autogen && configure, simply running make should suffice.

The -C tells configure to cache test results, which usually speeds
things up a bit.

That particular -W is useful because a) intermixing declarations with
statements is an easy thing to do accidentally, b) gcc doesn't warn
about it by default, and c) other compilers that don't support it are
still widespread.  If you're not using gcc, of course you shouldn't
specify that option.

Other -W options can be useful too, and patches are welcome to resolve
diagnostics; however, removing all possible warning messages, or
warnings with nonfree compilers, is explicitly not a goal.

This distribution also uses Gnulib (http://www.gnu.org/software/gnulib)
to share common files.  Gnulib files used in Texinfo are checked in to
the repository.  If you get automake/conf/etc. errors from ./autogen.sh,
please try getting a checkout of gnulib (in a separate directory from
the texinfo checkout), and then run, say,
  ../gnulib/gnulib-tool --add-import
in your top-level Texinfo directory.
(gnulib-tool is in the gnulib source tree.)

The currently-used gnulib modules and other gnulib information are
recorded in gnulib/m4/gnulib-cache.m4.  Given a source checkout of
gnulib, you can update the files with gnulib-tool --add-import.

When running gnulib-tool --add-import or otherwise adding modules, it is
necessary to check what files were added (e.g., run "svn status
build-aux gnulib") and add them to the repository with "svn add";
likewise for removals, likewise for adding new generated files
(typically gnulib/lib/foo.h from foo.h.in) to the ignore list (e.g.,
  svn propedit svn:ignore gnulib/lib).



About running the Texinfo programs from a development source tree:

- Once the distribution is built, you can run the compiled programs
(info, install-info) out of the build tree without special settings; 
they don't try to read any installed data files.

- The texi2dvi script and texinfo.tex can be run as-is, since they 
are standalone and don't require compilation.  For the same reasons,
they are officially updated between full Texinfo releases, at
http://ftpmirror.gnu.org/texinfo.

- Regarding texi2any (aka makeinfo), you can run tp/texi2any.pl
directly.  This is the original source file for the program, so it's
convenient to be able to make changes and then run it.

To run it under a name that doesn't end in .pl, e.g., "makeinfo", you
can use an alias, or set the environment variable TEXINFO_DEV_SOURCE to 1,
or symlink to a trivial script along these lines:
#!/bin/sh
exec /YOUR/TEXINFO/CHECKOUT/tp/texi2any.pl "$@"



References for working on various parts of the system:

If you want to delve into making a new backend for the Perl makeinfo,
the documentation in tp/Texinfo/Convert/Converter.pm is a good starting
point, as it describes the existing backends and other places to look.

If you want to delve into texinfo.tex, a thorough plain TeX reference
is available under the GFDL:
  TeX by Topic - http://www.eijkhout.net/texbytopic/texbytopic.html
Another book on plain TeX, also available under the GFDL, is a GNU package:
  TeX for the Impatient - http://www.gnu.org/software/teximpatient/

The texindex program is implemented using the TexiWebJR literate
programming system, combining Texinfo and Awk
(https://github.com/arnoldrobbins/texiwebjr).  Running "make ti.pdf"
in the texindex/ subdirectory creates the printable form of the
program.  All the usual Texinfo output formats are possible.



Steps for making a release (pretest or official):

- First checks:
Ensure texinfo.tex, texi2dvi, and htmlxref.cnf are updated on ftp.gnu.org.
Ensure TXI_XLATE in doc/Makefile.am matches actual file list.
Check that TEXINFO_DTD_VERSION has been updated to the next version in
  configure.ac if the DTD has been modified since the last release.

- When close to official release:
special pleading with bug-texinfo / beebe / platform-testers to try.
check OpenCSW build reports at
  https://buildfarm.opencsw.org/buildbot/waterfall?category=texinfo
try groff.texinfo from groff source repo.

- Official releases only:
version number in texi2dvi, texi2pdf, txirefcard.tex.
version and date in NEWS.
(cd tp && ./maintain/change_perl_modules_version.sh auto)
sync texi2html/test/ results with tp/.
ensure building standalone modules for CPAN still works.
make V=1 pdf and fix underfull/overfull boxes.

- Changes to sources:
update version in configure.ac, notice in ChangeLog.
gnulib-tool --add-import  # for pretest
util/srclist-txi          # for pretest 
pod2man Pod-Simple-Texinfo/pod2texi.pl >man/pod2texi.1 # until we fix deps
make
make po-check             # update po/POTFILES.in as needed
make update-po            # both po and po_document needed, build a dist first
make -C doc wwwdoc-build  # and -install for official (and eventual commit)
make long-checks          # distcheck does not do this
(export MALLOC_CHECK_=2; make distcheck)  # repeat until clean
svn commit                # when clean, then distcheck to be sure
#create diff for official, see below 

- To do the actual upload:
pkg=texinfo
ver=5.2

then do one of:
gnupload --to alpha.gnu.org:$pkg $pkg-$ver.tar.xz                #pretest
gnupload --to ftp.gnu.org:$pkg   $pkg-$ver.tar.{gz,xz} *.diff.xz #official
  (also gnupload texi2dvi at each official release.
   The standalone texinfo.tex should already be up to date, but check.)

and the corresponding, to save in local archives:
mv -v $pkg-$ver.tar.xz* $misc/archive/$pkg/alpha/                #pretest
mv -v $pkg-$ver.tar.{gz,xz}* *.diff.xz $misc/archive/$pkg/prod/  #official

For pretest release, send announcement to bug-texinfo and
bcc coordinator@translationproject.org.

- When official release is out there ...
update home page (texinfo.html) and commit as needed.
update manual (make wwwdoc-install, cvs commit), including:
  pod2html $txi/Pod-Simple-Texinfo/pod2texi.pl \
  | fgrep -v 'rev="made"' >manual/pod2texi.html

#  ... dtd version check:
TEXINFO_DTD_VERSION should already have been updated to $ver in
configure.ac if it was needed, but check again.  See comments in
configure.ac, and run (at the top level) make dtd-check.

# ... set up dtd directory on web pages:
cd $HOME/gnu/www/texinfo/dtd # or wherever webpages checkout is
mkdir $ver && cvs add $ver
cp $tutil/texinfo.dtd $ver
cvs add -kb $ver $ver/texinfo.dtd
cvs commit -m$ver $ver

# If -kb is forgotten, CVS will do its own $Id expansion.
# Recover by editing and committing a new version of texinfo.dtd in svn,
# copying it again to the cvs dir, then:
# cvs admin -kb texinfo.dtd; cvs update -A.
# (See the "How to store binary files" node in the CVS manual.)

#  ... tag source tree:
svn copy -r 5382 -m'texinfo_5_2 tag based on r5382' \
  svn+ssh://svn.savannah.gnu.org/texinfo/trunk \
  svn+ssh://svn.savannah.gnu.org/texinfo/tags/texinfo_5_2
[for karl: /srv/svn/texinfo]  

  ... update texinfo at tug.org (contact root@tug.org); from last time:
prev=5.1
ver=5.2
cd ~ftp/tex
rm -rf texinfo-$prev*
cp ~/texinfo-$ver.tar.{gz,xz} .
tar xzf texinfo-$ver.tar.gz
ln -s texinfo-$ver.tar.gz texinfo.tar.gz
!!:gs/gz/xz
relink texinfo $ver

  ... announcements:
send announcement to info-gnu,
  cc bug-texinfo and bcc coordinator@translationproject.org.
news item at savannah.

  ... make diffs at official release:
prev=5.1
ver=5.2
cd $misc/archive/$pkg/prod
tar xf $txi/texinfo-$ver.tar.gz
tar xf texinfo-$prev.tar.gz
diff -Nrc2 texinfo-$prev texinfo-$ver | xz >texinfo-$prev-$ver.diff.xz
ls -l !$
gnupload --to ftp.gnu.org:texinfo !$
rm -rf texinfo-$ver texinfo-$prev
ro texinfo-*$ver*
  
  ... [personal for karl] move mail:
mv ~/mail/txi.done $ver.mail
gzip $ver.mail
