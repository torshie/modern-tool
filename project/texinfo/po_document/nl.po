# Dutch translations of GNU Texinfo document strings.
# Copyright (C) 2013 Free Software Foundation, Inc.
# This file is distributed under the same license as the texinfo_document package.
#
# To Lillian and her handkerchief.
#
# Benno Schulenberg <benno@vertaalt.nl>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: texinfo_document-5.0\n"
"Report-Msgid-Bugs-To: bug-texinfo@gnu.org\n"
"POT-Creation-Date: 2015-06-26 13:52+0100\n"
"PO-Revision-Date: 2013-02-27 20:31+0100\n"
"Last-Translator: Benno Schulenberg <benno@vertaalt.nl>\n"
"Language-Team: Dutch <vertaling@vrijschrift.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.0\n"

#: tp/Texinfo/Common.pm:714 tp/Texinfo/Common.pm:719
msgid "Function"
msgstr "Functie"

#: tp/Texinfo/Common.pm:715
msgid "Macro"
msgstr "Macro"

#: tp/Texinfo/Common.pm:716
msgid "Special Form"
msgstr "Speciale vorm"

#: tp/Texinfo/Common.pm:717 tp/Texinfo/Common.pm:720
msgid "Variable"
msgstr "Variabele"

#: tp/Texinfo/Common.pm:718
msgid "User Option"
msgstr "Gebruikeroptie"

#: tp/Texinfo/Common.pm:721 tp/Texinfo/Common.pm:722
msgid "Instance Variable"
msgstr "Instantiatievariabele"

#: tp/Texinfo/Common.pm:723 tp/Texinfo/Common.pm:724
msgid "Method"
msgstr "Methode"

#: tp/Texinfo/Common.pm:1196
#, perl-brace-format
msgid "{category} on {class}"
msgstr "{category} over {class}"

#: tp/Texinfo/Common.pm:1203
#, perl-brace-format
msgid "{category} of {class}"
msgstr "{category} van {class}"

#: tp/Texinfo/Common.pm:1220
#, perl-brace-format
msgid "{month} {day}, {year}"
msgstr "{day} {month} {year}"

#: tp/Texinfo/Common.pm:1251 tp/Texinfo/Convert/Plaintext.pm:1112
#: tp/Texinfo/Convert/HTML.pm:515
#, perl-brace-format
msgid "Appendix {number} {section_title}"
msgstr "Appendix {number} {section_title}"

#: tp/Texinfo/Common.pm:1255 tp/Texinfo/Convert/Plaintext.pm:1117
#: tp/Texinfo/Convert/HTML.pm:521
#, perl-brace-format
msgid "{number} {section_title}"
msgstr "{number} {section_title}"

#: tp/Texinfo/Common.pm:1390
#, perl-brace-format
msgid "{float_type} {float_number}: "
msgstr "{float_type} {float_number}: "

#: tp/Texinfo/Common.pm:1394
#, perl-brace-format
msgid "{float_type}: "
msgstr "{float_type}: "

#: tp/Texinfo/Common.pm:1399
#, perl-brace-format
msgid "{float_type} {float_number}\n"
msgstr "{float_type} {float_number}\n"

#: tp/Texinfo/Common.pm:1403
#, perl-brace-format
msgid "{float_type}\n"
msgstr "{float_type}\n"

#: tp/Texinfo/Common.pm:1409
#, perl-brace-format
msgid "{float_number}: "
msgstr "{float_number}: "

#: tp/Texinfo/Common.pm:1412
#, perl-brace-format
msgid "{float_number}\n"
msgstr "{float_number}\n"

#: tp/Texinfo/Parser.pm:2981
#, perl-brace-format
msgid "{name} on {class}"
msgstr "{name} over {class}"

#: tp/Texinfo/Parser.pm:2990
#, perl-brace-format
msgid "{name} of {class}"
msgstr "{name} van {class}"

#: tp/Texinfo/Convert/Converter.pm:110
msgid "error@arrow{}"
msgstr "error@arrow{}"

#: tp/Texinfo/Convert/Converter.pm:956
msgid "January"
msgstr "Januari"

#: tp/Texinfo/Convert/Converter.pm:957
msgid "February"
msgstr "Februari"

#: tp/Texinfo/Convert/Converter.pm:958
msgid "March"
msgstr "Maart"

#: tp/Texinfo/Convert/Converter.pm:959
msgid "April"
msgstr "April"

#: tp/Texinfo/Convert/Converter.pm:960
msgid "May"
msgstr "Mei"

#: tp/Texinfo/Convert/Converter.pm:961
msgid "June"
msgstr "Juni"

#: tp/Texinfo/Convert/Converter.pm:962
msgid "July"
msgstr "Juli"

#: tp/Texinfo/Convert/Converter.pm:963
msgid "August"
msgstr "Augustus"

#: tp/Texinfo/Convert/Converter.pm:964
msgid "September"
msgstr "September"

#: tp/Texinfo/Convert/Converter.pm:965
msgid "October"
msgstr "Oktober"

#: tp/Texinfo/Convert/Converter.pm:966
msgid "November"
msgstr "November"

#: tp/Texinfo/Convert/Converter.pm:967
msgid "December"
msgstr "December"

#: tp/Texinfo/Convert/Converter.pm:1018
#, perl-brace-format
msgid "{float_type} {float_number}"
msgstr "{float_type} {float_number}"

#: tp/Texinfo/Convert/Converter.pm:1022
#, perl-brace-format
msgid "{float_type}"
msgstr "{float_type}"

#: tp/Texinfo/Convert/Converter.pm:1026
#, perl-brace-format
msgid "{float_number}"
msgstr "{float_number}"

#: tp/Texinfo/Convert/DocBook.pm:825
#, perl-brace-format
msgid "See Info file @file{{myfile}}, node @samp{{mynode}}"
msgstr "Zie Info-bestand @file{{myfile}}, pagina @samp{{mynode}}"

#: tp/Texinfo/Convert/DocBook.pm:830
#, perl-brace-format
msgid "See node @samp{{mynode}}"
msgstr "Zie pagina @samp{{mynode}}"

#: tp/Texinfo/Convert/DocBook.pm:834
#, perl-brace-format
msgid "See Info file @file{{myfile}}"
msgstr "Zie Info-bestand @file{{myfile}}"

#: tp/Texinfo/Convert/DocBook.pm:874
#, perl-brace-format
msgid "section ``{section_name}'' in @cite{{book}}"
msgstr "sectie “{section_name}” in @cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:879
#, perl-brace-format
msgid "See section ``{section_name}'' in @cite{{book}}"
msgstr "Zie sectie “{section_name}” in @cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:884
#, perl-brace-format
msgid "see section ``{section_name}'' in @cite{{book}}"
msgstr "zie sectie “{section_name}” in @cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:891 tp/Texinfo/Convert/HTML.pm:3287
#, perl-brace-format
msgid "@cite{{book}}"
msgstr "@cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:895 tp/Texinfo/Convert/HTML.pm:3264
#, perl-brace-format
msgid "See @cite{{book}}"
msgstr "Zie @cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:899 tp/Texinfo/Convert/HTML.pm:3241
#, perl-brace-format
msgid "see @cite{{book}}"
msgstr "zie @cite{{book}}"

#: tp/Texinfo/Convert/DocBook.pm:914
#, perl-brace-format
msgid "{title_ref}"
msgstr "{title_ref}"

#: tp/Texinfo/Convert/DocBook.pm:919
#, perl-brace-format
msgid "See {title_ref}"
msgstr "Zie {title_ref}"

#: tp/Texinfo/Convert/DocBook.pm:924
#, perl-brace-format
msgid "see {title_ref}"
msgstr "zie {title_ref}"

#: tp/Texinfo/Convert/DocBook.pm:1061 tp/Texinfo/Convert/Plaintext.pm:2204
#, perl-brace-format
msgid "{abbr_or_acronym} ({explanation})"
msgstr "{abbr_or_acronym} ({explanation})"

#: tp/Texinfo/Convert/DocBook.pm:1222 tp/Texinfo/Convert/Plaintext.pm:2390
#: tp/Texinfo/Convert/HTML.pm:3593
#, perl-brace-format
msgid "@b{{quotation_arg}:} "
msgstr "@b{{quotation_arg}:} "

#: tp/Texinfo/Convert/Plaintext.pm:1344
msgid "(outside of any node)"
msgstr "(buiten alle pagina's)"

#: tp/Texinfo/Convert/Plaintext.pm:1961
#, perl-brace-format
msgid "{name} @url{{email}}"
msgstr "{name} @url{{email}}"

#: tp/Texinfo/Convert/Plaintext.pm:1964
#, perl-brace-format
msgid "@url{{email}}"
msgstr "@url{{email}}"

#: tp/Texinfo/Convert/Plaintext.pm:1986
#, perl-brace-format
msgid "{text} ({url})"
msgstr "{text} ({url})"

#: tp/Texinfo/Convert/Plaintext.pm:1991
#, perl-brace-format
msgid "@t{<{url}>}"
msgstr "@t{<{url}>}"

#: tp/Texinfo/Convert/Plaintext.pm:2315 tp/Texinfo/Convert/HTML.pm:1439
#, perl-brace-format
msgid "@{No value for `{value}'@}"
msgstr "@{Geen waarde voor '{value}'@}"

#: tp/Texinfo/Convert/Plaintext.pm:2839
#, perl-brace-format
msgid "@tie{ }-- {category}: {name} {arguments}"
msgstr "@tie{ }-- {category}: {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2844
#, perl-brace-format
msgid "@tie{ }-- {category}: {name}"
msgstr "@tie{ }-- {category}: {name}"

#: tp/Texinfo/Convert/Plaintext.pm:2857
#, perl-brace-format
msgid "@tie{ }-- {category}:@*{type}@*{name} {arguments}"
msgstr "@tie{ }-- {category}:@*{type}@*{name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2860
#, perl-brace-format
msgid "@tie{ }-- {category}: {type} {name} {arguments}"
msgstr "@tie{ }-- {category}: {type} {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2869
#, perl-brace-format
msgid "@tie{ }-- {category}:@*{type}@*{name}"
msgstr "@tie{ }-- {category}:@*{type}@*{name}"

#: tp/Texinfo/Convert/Plaintext.pm:2872
#, perl-brace-format
msgid "@tie{ }-- {category}: {type} {name}"
msgstr "@tie{ }-- {category}: {type} {name}"

#: tp/Texinfo/Convert/Plaintext.pm:2880
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {name} {arguments}"
msgstr "@tie{ }-- {category} van {class}: {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2886
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {name}"
msgstr "@tie{ }-- {category} van {class}: {name}"

#: tp/Texinfo/Convert/Plaintext.pm:2895
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {name} {arguments}"
msgstr "@tie{ }-- {category} over {class}: {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2901
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {name}"
msgstr "@tie{ }-- {category} over {class}: {name}"

#: tp/Texinfo/Convert/Plaintext.pm:2916
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}:@*{type}@*{name} {arguments}"
msgstr "@tie{ }-- {category} over {class}:@*{type}@*{name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2920
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {type} {name} {arguments}"
msgstr "@tie{ }-- {category} over {class}: {type} {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2931
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}:@*{type}@*{name}"
msgstr "@tie{ }-- {category} over {class}:@*{type}@*{name}"

#: tp/Texinfo/Convert/Plaintext.pm:2935
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {type} {name}"
msgstr "@tie{ }-- {category} over {class}: {type} {name}"

#: tp/Texinfo/Convert/Plaintext.pm:2949
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}:@*{type}@*{name} {arguments}"
msgstr "@tie{ }-- {category} van {class}:@*{type}@*{name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2953
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {type} {name} {arguments}"
msgstr "@tie{ }-- {category} van {class}: {type} {name} {arguments}"

#: tp/Texinfo/Convert/Plaintext.pm:2964
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}:@*{type}@*{name}"
msgstr "@tie{ }-- {category} van {class}:@*{type}@*{name}"

#: tp/Texinfo/Convert/Plaintext.pm:2968
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {type} {name}"
msgstr "@tie{ }-- {category} van{class}: {type} {name}"

#: tp/Texinfo/Convert/Plaintext.pm:3279 tp/Texinfo/Convert/HTML.pm:2853
#, perl-brace-format
msgid "@center --- @emph{{author}}\n"
msgstr "@center --- @emph{{author}}\n"

#: tp/Texinfo/Convert/HTML.pm:732 tp/Texinfo/Convert/HTML.pm:788
msgid "Top"
msgstr "Top"

#: tp/Texinfo/Convert/HTML.pm:733 tp/Texinfo/Convert/HTML.pm:789
msgid "Contents"
msgstr "Inhoud"

#: tp/Texinfo/Convert/HTML.pm:734 tp/Texinfo/Convert/HTML.pm:790
msgid "Overview"
msgstr "Overzicht"

#: tp/Texinfo/Convert/HTML.pm:735 tp/Texinfo/Convert/HTML.pm:766
#: tp/Texinfo/Convert/HTML.pm:791
msgid "Index"
msgstr "Index"

# Met een hoofdletter want komt aan begin van zin, en voor "beleid" of "prioriteit".
#: tp/Texinfo/Convert/HTML.pm:737
msgid "current"
msgstr "huidige"

#: tp/Texinfo/Convert/HTML.pm:740 tp/Texinfo/Convert/HTML.pm:796
msgid "Prev"
msgstr "Vrg"

#: tp/Texinfo/Convert/HTML.pm:741
msgid " Up "
msgstr " Op "

#: tp/Texinfo/Convert/HTML.pm:742 tp/Texinfo/Convert/HTML.pm:746
#: tp/Texinfo/Convert/HTML.pm:798
msgid "Next"
msgstr "Volgende"

#: tp/Texinfo/Convert/HTML.pm:744 tp/Texinfo/Convert/HTML.pm:797
msgid "Up"
msgstr "Omhoog"

#: tp/Texinfo/Convert/HTML.pm:748
msgid "Previous"
msgstr "Voorgaande"

#: tp/Texinfo/Convert/HTML.pm:749
msgid "Forward node"
msgstr "Vooruit"

#: tp/Texinfo/Convert/HTML.pm:750
msgid "Back node"
msgstr "Terug"

#: tp/Texinfo/Convert/HTML.pm:756
msgid "Next file"
msgstr "Volgend bestand"

#: tp/Texinfo/Convert/HTML.pm:757
msgid "Previous file"
msgstr "Voorgaand bestand"

#: tp/Texinfo/Convert/HTML.pm:763
msgid "Cover (top) of document"
msgstr "Omslag van document (top)"

#: tp/Texinfo/Convert/HTML.pm:764
msgid "Table of contents"
msgstr "Inhoudsopgave"

#: tp/Texinfo/Convert/HTML.pm:765
msgid "Short table of contents"
msgstr "Korte inhoudsopgave"

#: tp/Texinfo/Convert/HTML.pm:767
msgid "Current section"
msgstr "Huidige sectie"

#: tp/Texinfo/Convert/HTML.pm:768
msgid "Previous section in reading order"
msgstr "Voorgaande sectie in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:769
msgid "Beginning of this chapter or previous chapter"
msgstr "Begin van dit (of voorgaand) hoofdstuk"

#: tp/Texinfo/Convert/HTML.pm:770
msgid "Previous section on same level"
msgstr "Voorgaande sectie op zelfde niveau"

#: tp/Texinfo/Convert/HTML.pm:771
msgid "Up section"
msgstr "Bovenliggende sectie"

#: tp/Texinfo/Convert/HTML.pm:772
msgid "Next section on same level"
msgstr "Volgende sectie op zelfde niveau"

#: tp/Texinfo/Convert/HTML.pm:773
msgid "Up node"
msgstr "Bovenliggende pagina"

#: tp/Texinfo/Convert/HTML.pm:774
msgid "Next node"
msgstr "Volgende pagina"

#: tp/Texinfo/Convert/HTML.pm:775
msgid "Previous node"
msgstr "Voorgaande pagina"

#: tp/Texinfo/Convert/HTML.pm:776
msgid "Next node in node reading order"
msgstr "Volgende pagina in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:777
msgid "Previous node in node reading order"
msgstr "Voorgaande pagina in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:778
msgid "Next section in reading order"
msgstr "Volgende sectie in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:779
msgid "Next chapter"
msgstr "Volgende hoofdstuk"

#: tp/Texinfo/Convert/HTML.pm:780
msgid "About (help)"
msgstr "Info (hulp)"

#: tp/Texinfo/Convert/HTML.pm:781
msgid "First section in reading order"
msgstr "Eerste sectie in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:782
msgid "Last section in reading order"
msgstr "Laatste sectie in leesvolgorde"

#: tp/Texinfo/Convert/HTML.pm:783
msgid "Forward section in next file"
msgstr "Sectie vooruit in volgend bestand"

#: tp/Texinfo/Convert/HTML.pm:784
msgid "Back section in previous file"
msgstr "Sectie terug in voorgaand bestand"

#: tp/Texinfo/Convert/HTML.pm:793
msgid "This"
msgstr "Deze"

#: tp/Texinfo/Convert/HTML.pm:794
msgid "Back"
msgstr "Terug"

#: tp/Texinfo/Convert/HTML.pm:795
msgid "FastBack"
msgstr "Snel-terug"

#: tp/Texinfo/Convert/HTML.pm:799
msgid "NodeUp"
msgstr "Pagina-omhoog"

#: tp/Texinfo/Convert/HTML.pm:800
msgid "NodeNext"
msgstr "Volgende-pagina"

#: tp/Texinfo/Convert/HTML.pm:801
msgid "NodePrev"
msgstr "Voorgaande-pagina"

#: tp/Texinfo/Convert/HTML.pm:802
msgid "NodeForward"
msgstr "Pagina-vooruit"

#: tp/Texinfo/Convert/HTML.pm:803
msgid "NodeBack"
msgstr "Pagina-terug"

#: tp/Texinfo/Convert/HTML.pm:804
msgid "Forward"
msgstr "Vooruit"

#: tp/Texinfo/Convert/HTML.pm:805
msgid "FastForward"
msgstr "Snel-vooruit"

#: tp/Texinfo/Convert/HTML.pm:806
msgid "About"
msgstr "Info"

#: tp/Texinfo/Convert/HTML.pm:807
msgid "First"
msgstr "Eerste"

#: tp/Texinfo/Convert/HTML.pm:808
msgid "Last"
msgstr "Laatste"

#: tp/Texinfo/Convert/HTML.pm:809
msgid "NextFile"
msgstr "Volgend-bestand"

#: tp/Texinfo/Convert/HTML.pm:810
msgid "PrevFile"
msgstr "Voorgaand-bestand"

#: tp/Texinfo/Convert/HTML.pm:814
msgid "About This Document"
msgstr "Over dit document"

#: tp/Texinfo/Convert/HTML.pm:815
msgid "Table of Contents"
msgstr "Inhoudsopgave"

#: tp/Texinfo/Convert/HTML.pm:816
msgid "Short Table of Contents"
msgstr "Korte inhoudsopgave"

#: tp/Texinfo/Convert/HTML.pm:817
msgid "Footnotes"
msgstr "Voetnoten"

#: tp/Texinfo/Convert/HTML.pm:1505
#, perl-brace-format
msgid "{explained_string} ({explanation})"
msgstr "{explained_string} ({explanation})"

#: tp/Texinfo/Convert/HTML.pm:3152
#, perl-brace-format
msgid "see section {reference_name}"
msgstr "zie sectie {reference_name}"

#: tp/Texinfo/Convert/HTML.pm:3155
#, perl-brace-format
msgid "see {reference_name}"
msgstr "zie {reference_name}"

#: tp/Texinfo/Convert/HTML.pm:3160
#, perl-brace-format
msgid "See section {reference_name}"
msgstr "Zie sectie {reference_name}"

#: tp/Texinfo/Convert/HTML.pm:3163
#, perl-brace-format
msgid "See {reference_name}"
msgstr "Zie {reference_name}"

#: tp/Texinfo/Convert/HTML.pm:3167
#, perl-brace-format
msgid "{reference_name}"
msgstr "{reference_name}"

#: tp/Texinfo/Convert/HTML.pm:3229
#, perl-brace-format
msgid "see {reference} in @cite{{book}}"
msgstr "zie {reference} in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3233
#, perl-brace-format
msgid "see @cite{{book_reference}}"
msgstr "zie @cite{{book_reference}}"

#: tp/Texinfo/Convert/HTML.pm:3237
#, perl-brace-format
msgid "see `{section}' in @cite{{book}}"
msgstr "zie ‘{section}’ in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3244
#, perl-brace-format
msgid "see {reference}"
msgstr "zie {reference}"

#: tp/Texinfo/Convert/HTML.pm:3247
#, perl-brace-format
msgid "see `{section}'"
msgstr "zie ‘{section}’"

#: tp/Texinfo/Convert/HTML.pm:3252
#, perl-brace-format
msgid "See {reference} in @cite{{book}}"
msgstr "Zie {reference} in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3256
#, perl-brace-format
msgid "See @cite{{book_reference}}"
msgstr "Zie @cite{{book_reference}}"

#: tp/Texinfo/Convert/HTML.pm:3260
#, perl-brace-format
msgid "See `{section}' in @cite{{book}}"
msgstr "Zie ‘{section}’ in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3267
#, perl-brace-format
msgid "See {reference}"
msgstr "Zie {reference}"

#: tp/Texinfo/Convert/HTML.pm:3270
#, perl-brace-format
msgid "See `{section}'"
msgstr "Zie ‘{section}’"

#: tp/Texinfo/Convert/HTML.pm:3275
#, perl-brace-format
msgid "{reference} in @cite{{book}}"
msgstr "{reference} in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3279
#, perl-brace-format
msgid "@cite{{book_reference}}"
msgstr "@cite{{book_reference}}"

#: tp/Texinfo/Convert/HTML.pm:3283
#, perl-brace-format
msgid "`{section}' in @cite{{book}}"
msgstr "‘{section}’ in @cite{{book}}"

#: tp/Texinfo/Convert/HTML.pm:3290
#, perl-brace-format
msgid "{reference}"
msgstr "{reference}"

#: tp/Texinfo/Convert/HTML.pm:3293
#, perl-brace-format
msgid "`{section}'"
msgstr "‘{section}’"

#: tp/Texinfo/Convert/HTML.pm:3406
msgid "Jump to"
msgstr "Spring naar"

#: tp/Texinfo/Convert/HTML.pm:3414
msgid "Index Entry"
msgstr "Index-item"

#: tp/Texinfo/Convert/HTML.pm:3416
msgid "Section"
msgstr "Sectie"

#: tp/Texinfo/Convert/HTML.pm:4035
#, perl-brace-format
msgid "{category}: @strong{{name}} @emph{{arguments}}"
msgstr "{category}: @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4040
#, perl-brace-format
msgid "{category}: @strong{{name}}"
msgstr "{category}: @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4057
#, perl-brace-format
msgid "{category}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr "{category}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4061
#, perl-brace-format
msgid "{category}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr "{category}: @emph{{type}} @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4070
#, perl-brace-format
msgid "{category}:@* @emph{{type}}@* @strong{{name}}"
msgstr "{category}:@* @emph{{type}}@* @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4073
#, perl-brace-format
msgid "{category}: @emph{{type}} @strong{{name}}"
msgstr "{category}: @emph{{type}} @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4081
#, perl-brace-format
msgid "{category} of {class}: @strong{{name}} @emph{{arguments}}"
msgstr "{category} van {class}: @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4087
#, perl-brace-format
msgid "{category} of {class}: @strong{{name}}"
msgstr "{category} van {class}: @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4096
#, perl-brace-format
msgid "{category} on {class}: @strong{{name}} @emph{{arguments}}"
msgstr "{category} over {class}: @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4102
#, perl-brace-format
msgid "{category} on {class}: @strong{{name}}"
msgstr "{category} over {class}: @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4117
#, perl-brace-format
msgid ""
"{category} on {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr ""
"{category} over {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4121
#, perl-brace-format
msgid "{category} on {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr ""
"{category} over {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4132
#, perl-brace-format
msgid "{category} on {class}:@* @emph{{type}}@* @strong{{name}}"
msgstr "{category} over {class}:@* @emph{{type}}@* @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4136
#, perl-brace-format
msgid "{category} on {class}: @emph{{type}} @strong{{name}}"
msgstr "{category} over {class}: @emph{{type}} @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4150
#, perl-brace-format
msgid ""
"{category} of {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr ""
"{category} van {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4154
#, perl-brace-format
msgid "{category} of {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr ""
"{category} van {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"

#: tp/Texinfo/Convert/HTML.pm:4165
#, perl-brace-format
msgid "{category} of {class}:@* @emph{{type}}@* @strong{{name}}"
msgstr "{category} van {class}:@* @emph{{type}}@* @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:4169
#, perl-brace-format
msgid "{category} of {class}: @emph{{type}} @strong{{name}}"
msgstr "{category} van {class}: @emph{{type}} @strong{{name}}"

#: tp/Texinfo/Convert/HTML.pm:6096
#, perl-brace-format
msgid ""
"This document was generated on @emph{@today{}} using "
"@uref{{program_homepage}, @emph{{program}}}."
msgstr ""
"Dit document werd gegenereerd op @emph{@today{}} met behulp van "
"@uref{{program_homepage}, @emph{{program}}}."

#: tp/Texinfo/Convert/HTML.pm:6101
msgid "This document was generated on @emph{@today{}}."
msgstr "Dit document werd gegenereerd op @emph{@today{}}."

#: tp/Texinfo/Convert/HTML.pm:6140
#, perl-brace-format
msgid "{title}: {element_text}"
msgstr "{title}: {element_text}"

#: tp/Texinfo/Convert/HTML.pm:6284
#, perl-brace-format
msgid "The node you are looking for is at {href}."
msgstr "De pagina die u zoekt bevindt zich op {href}."

#: tp/Texinfo/Convert/HTML.pm:6353
msgid "  The buttons in the navigation panels have the following meaning:"
msgstr "  De knoppen in de navigatiepanelen hebben de volgende functies:"

#: tp/Texinfo/Convert/HTML.pm:6359
msgid "Button"
msgstr "Knop"

#: tp/Texinfo/Convert/HTML.pm:6360
msgid "Name"
msgstr "Naam"

#: tp/Texinfo/Convert/HTML.pm:6361
msgid "Go to"
msgstr "Gaat naar"

#: tp/Texinfo/Convert/HTML.pm:6362
msgid "From 1.2.3 go to"
msgstr "Gaat van 1.2.3 naar"

#: tp/Texinfo/Convert/HTML.pm:6388
msgid ""
"  where the @strong{ Example } assumes that the current position is at "
"@strong{ Subsubsection One-Two-Three } of a document of the following "
"structure:"
msgstr ""
"  waar het @strong{ Voorbeeld } aanneemt dat de huidige positie "
"@strong{ subsubsectie één-twee-drie } is in een document met de volgende "
"structuur:"

#: tp/Texinfo/Convert/HTML.pm:6398
msgid "Section One"
msgstr "Sectie één"

#: tp/Texinfo/Convert/HTML.pm:6400
msgid "Subsection One-One"
msgstr "Subsectie één-één"

#: tp/Texinfo/Convert/HTML.pm:6407
msgid "Subsection One-Two"
msgstr "Subsectie één-twee"

#: tp/Texinfo/Convert/HTML.pm:6409
msgid "Subsubsection One-Two-One"
msgstr "Subsectie één-twee-één"

#: tp/Texinfo/Convert/HTML.pm:6410
msgid "Subsubsection One-Two-Two"
msgstr "Subsectie één-twee-twee"

#: tp/Texinfo/Convert/HTML.pm:6411
msgid "Subsubsection One-Two-Three"
msgstr "Subsectie één-twee-drie"

#: tp/Texinfo/Convert/HTML.pm:6413
msgid "Current Position"
msgstr "Huidige positie"

#: tp/Texinfo/Convert/HTML.pm:6414
msgid "Subsubsection One-Two-Four"
msgstr "Subsectie één-twee-vier"

#: tp/Texinfo/Convert/HTML.pm:6417
msgid "Subsection One-Three"
msgstr "Subsectie één-drie"

#: tp/Texinfo/Convert/HTML.pm:6424
msgid "Subsection One-Four"
msgstr "Subsectie één-vier"

#: tp/Texinfo/Convert/HTML.pm:6828
msgid "Untitled Document"
msgstr "Naamloos document"
