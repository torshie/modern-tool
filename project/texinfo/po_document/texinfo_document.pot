# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: GNU texinfo 6.0\n"
"Report-Msgid-Bugs-To: bug-texinfo@gnu.org\n"
"POT-Creation-Date: 2015-06-26 13:52+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: tp/Texinfo/Common.pm:714 tp/Texinfo/Common.pm:719
msgid "Function"
msgstr ""

#: tp/Texinfo/Common.pm:715
msgid "Macro"
msgstr ""

#: tp/Texinfo/Common.pm:716
msgid "Special Form"
msgstr ""

#: tp/Texinfo/Common.pm:717 tp/Texinfo/Common.pm:720
msgid "Variable"
msgstr ""

#: tp/Texinfo/Common.pm:718
msgid "User Option"
msgstr ""

#: tp/Texinfo/Common.pm:721 tp/Texinfo/Common.pm:722
msgid "Instance Variable"
msgstr ""

#: tp/Texinfo/Common.pm:723 tp/Texinfo/Common.pm:724
msgid "Method"
msgstr ""

#: tp/Texinfo/Common.pm:1196
#, perl-brace-format
msgid "{category} on {class}"
msgstr ""

#: tp/Texinfo/Common.pm:1203
#, perl-brace-format
msgid "{category} of {class}"
msgstr ""

#: tp/Texinfo/Common.pm:1220
#, perl-brace-format
msgid "{month} {day}, {year}"
msgstr ""

#: tp/Texinfo/Common.pm:1251 tp/Texinfo/Convert/Plaintext.pm:1112
#: tp/Texinfo/Convert/HTML.pm:515
#, perl-brace-format
msgid "Appendix {number} {section_title}"
msgstr ""

#: tp/Texinfo/Common.pm:1255 tp/Texinfo/Convert/Plaintext.pm:1117
#: tp/Texinfo/Convert/HTML.pm:521
#, perl-brace-format
msgid "{number} {section_title}"
msgstr ""

#: tp/Texinfo/Common.pm:1390
#, perl-brace-format
msgid "{float_type} {float_number}: "
msgstr ""

#: tp/Texinfo/Common.pm:1394
#, perl-brace-format
msgid "{float_type}: "
msgstr ""

#: tp/Texinfo/Common.pm:1399
#, perl-brace-format
msgid "{float_type} {float_number}\n"
msgstr ""

#: tp/Texinfo/Common.pm:1403
#, perl-brace-format
msgid "{float_type}\n"
msgstr ""

#: tp/Texinfo/Common.pm:1409
#, perl-brace-format
msgid "{float_number}: "
msgstr ""

#: tp/Texinfo/Common.pm:1412
#, perl-brace-format
msgid "{float_number}\n"
msgstr ""

#: tp/Texinfo/Parser.pm:2981
#, perl-brace-format
msgid "{name} on {class}"
msgstr ""

#: tp/Texinfo/Parser.pm:2990
#, perl-brace-format
msgid "{name} of {class}"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:110
msgid "error@arrow{}"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:956
msgid "January"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:957
msgid "February"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:958
msgid "March"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:959
msgid "April"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:960
msgid "May"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:961
msgid "June"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:962
msgid "July"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:963
msgid "August"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:964
msgid "September"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:965
msgid "October"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:966
msgid "November"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:967
msgid "December"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:1018
#, perl-brace-format
msgid "{float_type} {float_number}"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:1022
#, perl-brace-format
msgid "{float_type}"
msgstr ""

#: tp/Texinfo/Convert/Converter.pm:1026
#, perl-brace-format
msgid "{float_number}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:825
#, perl-brace-format
msgid "See Info file @file{{myfile}}, node @samp{{mynode}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:830
#, perl-brace-format
msgid "See node @samp{{mynode}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:834
#, perl-brace-format
msgid "See Info file @file{{myfile}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:874
#, perl-brace-format
msgid "section ``{section_name}'' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:879
#, perl-brace-format
msgid "See section ``{section_name}'' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:884
#, perl-brace-format
msgid "see section ``{section_name}'' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:891 tp/Texinfo/Convert/HTML.pm:3287
#, perl-brace-format
msgid "@cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:895 tp/Texinfo/Convert/HTML.pm:3264
#, perl-brace-format
msgid "See @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:899 tp/Texinfo/Convert/HTML.pm:3241
#, perl-brace-format
msgid "see @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:914
#, perl-brace-format
msgid "{title_ref}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:919
#, perl-brace-format
msgid "See {title_ref}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:924
#, perl-brace-format
msgid "see {title_ref}"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:1061 tp/Texinfo/Convert/Plaintext.pm:2204
#, perl-brace-format
msgid "{abbr_or_acronym} ({explanation})"
msgstr ""

#: tp/Texinfo/Convert/DocBook.pm:1222 tp/Texinfo/Convert/Plaintext.pm:2390
#: tp/Texinfo/Convert/HTML.pm:3593
#, perl-brace-format
msgid "@b{{quotation_arg}:} "
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:1344
msgid "(outside of any node)"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:1961
#, perl-brace-format
msgid "{name} @url{{email}}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:1964
#, perl-brace-format
msgid "@url{{email}}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:1986
#, perl-brace-format
msgid "{text} ({url})"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:1991
#, perl-brace-format
msgid "@t{<{url}>}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2315 tp/Texinfo/Convert/HTML.pm:1439
#, perl-brace-format
msgid "@{No value for `{value}'@}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2839
#, perl-brace-format
msgid "@tie{ }-- {category}: {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2844
#, perl-brace-format
msgid "@tie{ }-- {category}: {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2857
#, perl-brace-format
msgid "@tie{ }-- {category}:@*{type}@*{name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2860
#, perl-brace-format
msgid "@tie{ }-- {category}: {type} {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2869
#, perl-brace-format
msgid "@tie{ }-- {category}:@*{type}@*{name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2872
#, perl-brace-format
msgid "@tie{ }-- {category}: {type} {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2880
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2886
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2895
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2901
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2916
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}:@*{type}@*{name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2920
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {type} {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2931
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}:@*{type}@*{name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2935
#, perl-brace-format
msgid "@tie{ }-- {category} on {class}: {type} {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2949
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}:@*{type}@*{name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2953
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {type} {name} {arguments}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2964
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}:@*{type}@*{name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:2968
#, perl-brace-format
msgid "@tie{ }-- {category} of {class}: {type} {name}"
msgstr ""

#: tp/Texinfo/Convert/Plaintext.pm:3279 tp/Texinfo/Convert/HTML.pm:2853
#, perl-brace-format
msgid "@center --- @emph{{author}}\n"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:732 tp/Texinfo/Convert/HTML.pm:788
msgid "Top"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:733 tp/Texinfo/Convert/HTML.pm:789
msgid "Contents"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:734 tp/Texinfo/Convert/HTML.pm:790
msgid "Overview"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:735 tp/Texinfo/Convert/HTML.pm:766
#: tp/Texinfo/Convert/HTML.pm:791
msgid "Index"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:737
msgid "current"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:740 tp/Texinfo/Convert/HTML.pm:796
msgid "Prev"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:741
msgid " Up "
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:742 tp/Texinfo/Convert/HTML.pm:746
#: tp/Texinfo/Convert/HTML.pm:798
msgid "Next"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:744 tp/Texinfo/Convert/HTML.pm:797
msgid "Up"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:748
msgid "Previous"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:749
msgid "Forward node"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:750
msgid "Back node"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:756
msgid "Next file"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:757
msgid "Previous file"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:763
msgid "Cover (top) of document"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:764
msgid "Table of contents"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:765
msgid "Short table of contents"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:767
msgid "Current section"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:768
msgid "Previous section in reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:769
msgid "Beginning of this chapter or previous chapter"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:770
msgid "Previous section on same level"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:771
msgid "Up section"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:772
msgid "Next section on same level"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:773
msgid "Up node"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:774
msgid "Next node"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:775
msgid "Previous node"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:776
msgid "Next node in node reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:777
msgid "Previous node in node reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:778
msgid "Next section in reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:779
msgid "Next chapter"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:780
msgid "About (help)"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:781
msgid "First section in reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:782
msgid "Last section in reading order"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:783
msgid "Forward section in next file"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:784
msgid "Back section in previous file"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:793
msgid "This"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:794
msgid "Back"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:795
msgid "FastBack"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:799
msgid "NodeUp"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:800
msgid "NodeNext"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:801
msgid "NodePrev"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:802
msgid "NodeForward"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:803
msgid "NodeBack"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:804
msgid "Forward"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:805
msgid "FastForward"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:806
msgid "About"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:807
msgid "First"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:808
msgid "Last"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:809
msgid "NextFile"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:810
msgid "PrevFile"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:814
msgid "About This Document"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:815
msgid "Table of Contents"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:816
msgid "Short Table of Contents"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:817
msgid "Footnotes"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:1505
#, perl-brace-format
msgid "{explained_string} ({explanation})"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3152
#, perl-brace-format
msgid "see section {reference_name}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3155
#, perl-brace-format
msgid "see {reference_name}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3160
#, perl-brace-format
msgid "See section {reference_name}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3163
#, perl-brace-format
msgid "See {reference_name}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3167
#, perl-brace-format
msgid "{reference_name}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3229
#, perl-brace-format
msgid "see {reference} in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3233
#, perl-brace-format
msgid "see @cite{{book_reference}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3237
#, perl-brace-format
msgid "see `{section}' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3244
#, perl-brace-format
msgid "see {reference}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3247
#, perl-brace-format
msgid "see `{section}'"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3252
#, perl-brace-format
msgid "See {reference} in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3256
#, perl-brace-format
msgid "See @cite{{book_reference}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3260
#, perl-brace-format
msgid "See `{section}' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3267
#, perl-brace-format
msgid "See {reference}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3270
#, perl-brace-format
msgid "See `{section}'"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3275
#, perl-brace-format
msgid "{reference} in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3279
#, perl-brace-format
msgid "@cite{{book_reference}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3283
#, perl-brace-format
msgid "`{section}' in @cite{{book}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3290
#, perl-brace-format
msgid "{reference}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3293
#, perl-brace-format
msgid "`{section}'"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3406
msgid "Jump to"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3414
msgid "Index Entry"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:3416
msgid "Section"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4035
#, perl-brace-format
msgid "{category}: @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4040
#, perl-brace-format
msgid "{category}: @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4057
#, perl-brace-format
msgid "{category}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4061
#, perl-brace-format
msgid "{category}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4070
#, perl-brace-format
msgid "{category}:@* @emph{{type}}@* @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4073
#, perl-brace-format
msgid "{category}: @emph{{type}} @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4081
#, perl-brace-format
msgid "{category} of {class}: @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4087
#, perl-brace-format
msgid "{category} of {class}: @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4096
#, perl-brace-format
msgid "{category} on {class}: @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4102
#, perl-brace-format
msgid "{category} on {class}: @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4117
#, perl-brace-format
msgid ""
"{category} on {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4121
#, perl-brace-format
msgid "{category} on {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4132
#, perl-brace-format
msgid "{category} on {class}:@* @emph{{type}}@* @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4136
#, perl-brace-format
msgid "{category} on {class}: @emph{{type}} @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4150
#, perl-brace-format
msgid ""
"{category} of {class}:@* @emph{{type}}@* @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4154
#, perl-brace-format
msgid "{category} of {class}: @emph{{type}} @strong{{name}} @emph{{arguments}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4165
#, perl-brace-format
msgid "{category} of {class}:@* @emph{{type}}@* @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:4169
#, perl-brace-format
msgid "{category} of {class}: @emph{{type}} @strong{{name}}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6096
#, perl-brace-format
msgid ""
"This document was generated on @emph{@today{}} using "
"@uref{{program_homepage}, @emph{{program}}}."
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6101
msgid "This document was generated on @emph{@today{}}."
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6140
#, perl-brace-format
msgid "{title}: {element_text}"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6284
#, perl-brace-format
msgid "The node you are looking for is at {href}."
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6353
msgid "  The buttons in the navigation panels have the following meaning:"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6359
msgid "Button"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6360
msgid "Name"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6361
msgid "Go to"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6362
msgid "From 1.2.3 go to"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6388
msgid ""
"  where the @strong{ Example } assumes that the current position is at "
"@strong{ Subsubsection One-Two-Three } of a document of the following "
"structure:"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6398
msgid "Section One"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6400
msgid "Subsection One-One"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6407
msgid "Subsection One-Two"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6409
msgid "Subsubsection One-Two-One"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6410
msgid "Subsubsection One-Two-Two"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6411
msgid "Subsubsection One-Two-Three"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6413
msgid "Current Position"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6414
msgid "Subsubsection One-Two-Four"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6417
msgid "Subsection One-Three"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6424
msgid "Subsection One-Four"
msgstr ""

#: tp/Texinfo/Convert/HTML.pm:6828
msgid "Untitled Document"
msgstr ""
