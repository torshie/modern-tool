include build/config.mk

BUILD_DIR := $(abspath $(BUILD_DIR))

configure = cd $(patsubst %.configure,%,$@) \
		&& $(CURDIR)/project/$(patsubst %.configure,%,$(notdir $@))/configure \
		--prefix=$(INSTALL_PREFIX) $(1)

getname = $(patsubst %.configure,%,$(notdir $@))

prepare = mkdir -p $(BUILD_DIR)/$(getname) \
		&& find $(CURDIR)/project/$(getname) -type f -exec touch -d \
				'$(shell date --rfc-3339=date)' {} ';'

toolenv = CC=$(BUILD_DIR)/wrapper/gcc \
		CXX=$(BUILD_DIR)/wrapper/g++ \
		LD=$(BUILD_DIR)/wrapper/ld \
		CFLAGS=-I$(INSTALL_PREFIX)/include \
		CXXFLAGS=-I$(INSTALL_PREFIX)/include \
		LDFLAGS='-L$(INSTALL_PREFIX)/lib -Wl,-rpath=$(INSTALL_PREFIX)/lib'

rpathwrapper = $(BUILD_DIR)/wrapper/ld $(BUILD_DIR)/wrapper/gcc \
		$(BUILD_DIR)/wrapper/g++

all: gcc

gcc: $(BUILD_DIR)/gcc.install

gdb: $(BUILD_DIR)/gdb.install

llvm: $(BUILD_DIR)/llvm-stage1.install

%.install: %.configure
	env PATH=$(INSTALL_PREFIX)/bin:$(PATH) $(MAKE) -C $*
	env PATH=$(INSTALL_PREFIX)/bin:$(PATH) $(MAKE) -C $* install
	touch $< $@

$(BUILD_DIR)/cmake.install: $(BUILD_DIR)/cmake.configure
	env $(toolenv) MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
			PATH=$(BUILD_DIR)/wrapper:$(PATH) \
			$(MAKE) -C $(BUILD_DIR)/cmake
	env $(toolenv) MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
			PATH=$(BUILD_DIR)/wrapper:$(PATH) \
			$(MAKE) -C $(BUILD_DIR)/cmake install
	touch $< $@

$(BUILD_DIR)/llvm-stage1.install: $(BUILD_DIR)/llvm-stage1.configure
	env $(toolenv) MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
			$(MAKE) -C $(BUILD_DIR)/llvm-stage1
	env $(toolenv) MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
			$(MAKE) -C $(BUILD_DIR)/llvm-stage1 install
	touch $< $@

$(BUILD_DIR)/llvm.install: $(rpathwrapper) $(BUILD_DIR)/llvm-src \
		$(BUILD_DIR)/llvm-stage1.install $(BUILD_DIR)/llvm-stage2.install \
		$(BUILD_DIR)/llvm-stage3.install
	touch $@

$(BUILD_DIR)/llvm-src:
	rm -rf $(BUILD_DIR)/llvm
	mkdir -p $(BUILD_DIR)
	cp -r $(CURDIR)/project/llvm $(BUILD_DIR)/llvm
	cp -r $(CURDIR)/project/cfe $(BUILD_DIR)/llvm/tools/clang
	cp -r $(CURDIR)/project/clang-tools-extra \
			$(BUILD_DIR)/llvm/tools/clang/tools/clang-tools-extra
	cp -r $(CURDIR)/project/compiler-rt $(BUILD_DIR)/llvm/projects/compiler-rt
	cp -r $(CURDIR)/project/libcxx $(BUILD_DIR)/llvm/projects/libcxx
	cp -r $(CURDIR)/project/libcxxabi $(BUILD_DIR)/llvm/projects/libcxxabi
	cp -r $(CURDIR)/project/libunwind $(BUILD_DIR)/llvm/projects/libunwind
	cp -r $(CURDIR)/project/lld $(BUILD_DIR)/llvm/tools/lld
	touch $@

$(BUILD_DIR)/llvm-stage1.configure: $(BUILD_DIR)/llvm-src \
		$(BUILD_DIR)/cmake.install $(BUILD_DIR)/python.install $(rpathwrapper)
	rm -rf $(BUILD_DIR)/llvm-stage1
	mkdir -p $(BUILD_DIR)/llvm-stage1
	cd $(BUILD_DIR)/llvm-stage1 \
			&& env $(toolenv) MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
					$(INSTALL_PREFIX)/bin/cmake \
					-DCMAKE_INSTALL_PREFIX=$(BUILD_DIR)/stage1-install \
					-DCMAKE_BUILD_TYPE=Release \
					-DLLVM_TARGETS_TO_BUILD=host \
					-DLLVM_ENABLE_EH=ON \
					-DLLVM_ENABLE_RTTI=ON \
					-DLLVM_BINUTILS_INCDIR=$(INSTALL_PREFIX)/include \
					-DPYTHON_EXECUTABLE=$(INSTALL_PREFIX)/bin/python \
					-DGCC_INSTALL_PREFIX=$(INSTALL_PREFIX) \
					../llvm

$(BUILD_DIR)/gcc.configure: ldflags = '-Wl,-rpath,$(INSTALL_PREFIX)/lib \
		-static-libgcc -static-libstdc++'
$(BUILD_DIR)/gcc.configure: $(BUILD_DIR)/isl.install \
		$(BUILD_DIR)/gmp.install $(BUILD_DIR)/mpfr.install \
		$(BUILD_DIR)/mpc.install $(BUILD_DIR)/binutils.install
	$(prepare)
	cd $(BUILD_DIR)/gcc && $(CURDIR)/project/gcc/configure \
			--prefix=$(INSTALL_PREFIX) --build=$(TARGET_TRIPLE) \
			--host=$(TARGET_TRIPLE) --target=$(TARGET_TRIPLE) \
			--with-gmp=$(INSTALL_PREFIX) --with-mpfr=$(INSTALL_PREFIX) \
			--with-mpc=$(INSTALL_PREFIX) --with-isl=$(INSTALL_PREFIX) \
			--with-sysroot=/ --disable-nls --disable-werror --enable-shared \
			--enable-linker-build-id --enable-threads=posix \
			--enable-clocale=gnu --enable-libstdcxx --enable-libsanitizer \
			--enable-libgomp --enable-libssp --disable-vtable-verify \
			--enable-plugin --disable-multilib --enable-bootstrap \
			--enable-languages=c++,c --enable-lto --enable-__cxa_atexit \
			--with-diagnostics-color=auto --enable-libstdcxx-threads \
			--without-system-libunwind --with-boot-ldflags=$(ldflags) \
			--with-stage1-ldflags=$(ldflags) \
			LDFLAGS=$(ldflags)

$(BUILD_DIR)/binutils.configure: $(BUILD_DIR)/bison.install \
			$(BUILD_DIR)/flex.install
	$(prepare)
	$(call configure, --build=$(TARGET_TRIPLE) --host=$(TARGET_TRIPLE) \
			--target=$(TARGET_TRIPLE) --with-sysroot=/ --disable-nls \
			--enable-gold --enable-lto --enable-plugins \
			YACC=$(INSTALL_PREFIX)/bin/yacc \
			PATH=$(INSTALL_PREFIX)/bin:$(PATH))

$(BUILD_DIR)/gmp.configure:
	$(prepare)
	$(call configure, --enable-shared --disable-static --enable-cxx)

$(BUILD_DIR)/mpfr.configure: $(BUILD_DIR)/gmp.install
	$(prepare)
	$(call configure, --enable-shared --disable-static \
			--with-gmp=$(INSTALL_PREFIX))

$(BUILD_DIR)/mpc.configure: $(BUILD_DIR)/gmp.install \
		$(BUILD_DIR)/mpfr.install
	$(prepare)
	$(call configure, --enable-shared --disable-static \
			--with-gmp=$(INSTALL_PREFIX) --with-mpfr=$(INSTALL_PREFIX))

$(BUILD_DIR)/isl.configure: $(BUILD_DIR)/gmp.install
	$(prepare)
	$(call configure, --enable-shared --disable-static \
			--with-gmp=system --with-gmp-prefix=$(INSTALL_PREFIX))

$(BUILD_DIR)/bison.configure: $(BUILD_DIR)/texinfo.install
	$(prepare)
	$(call configure, --disable-nls --enable-threads=posix)

$(BUILD_DIR)/texinfo.configure:
	$(prepare)
	touch $(CURDIR)/project/texinfo/man/*.1
	touch $(CURDIR)/project/texinfo/man/*.5
	$(call configure, --disable-nls --enable-threads=posix)

$(BUILD_DIR)/python.configure: $(rpathwrapper) $(BUILD_DIR)/readline.install
	$(prepare)
	$(call configure, --enable-shared --enable-ipv6 --enable-unicode \
			--without-system-expat --without-system-ffi \
			--with-thread=posix --without-pymalloc --with-fpectl \
			--with-ensurepip=install \
			$(toolenv))

$(BUILD_DIR)/wrapper/%: $(BUILD_DIR)/gcc.install build/tool
	mkdir -p $(dir $@)
	sed 's#MTOOL_PLACEHOLDER#$(INSTALL_PREFIX)/bin/$*#' < build/tool > $@
	chmod +x $@

$(BUILD_DIR)/gdb.configure: $(BUILD_DIR)/python.install \
		$(BUILD_DIR)/ncurses.install $(BUILD_DIR)/readline.install \
		$(rpathwrapper)
	$(prepare)
	$(call configure, --build=$(TARGET_TRIPLE) --host=$(TARGET_TRIPLE) \
			--target=$(TARGET_TRIPLE) --disable-nls --enable-tui \
			--enable-gdbserver --without-python \
			--with-auto-load-dir='$$debugdir:$$datadir/auto-load' \
			--with-auto-load-safe-path='$$debugdir:$$datadir/auto-load' \
			--with-jit-reader-dir=$(INSTALL_PREFIX)/lib/gdb \
			--with-gdb-datadir=$(INSTALL_PREFIX)/lib/gdb \
			--with-system-readline=$(INSTALL_PREFIX) \
			--with-curses=$(INSTALL_PREFIX) $(toolenv))

$(BUILD_DIR)/ncurses.configure: $(rpathwrapper)
	$(prepare)
	$(call configure, --with-shared --with-cxx-shared \
			$(toolenv) CPPFLAGS=-P)

$(BUILD_DIR)/readline.configure: $(rpathwrapper)
	$(prepare)
	$(call configure, $(toolenv))

# CMAKE has a handle rolled build system which doesn't support autotool
# conventions, so the build instruction are complicated
$(BUILD_DIR)/cmake.configure: $(rpathwrapper)
	rm -rf $(BUILD_DIR)/cmake
	mkdir -p $(BUILD_DIR)
	cp -r $(CURDIR)/project/cmake $(BUILD_DIR)/cmake
	cd $(BUILD_DIR)/cmake && env $(toolenv) \
			MTOOL_INSTALL_PREFIX=$(INSTALL_PREFIX) \
			PATH=$(BUILD_DIR)/wrapper:$(PATH) \
			./configure --prefix=$(INSTALL_PREFIX)
	touch $@

$(BUILD_DIR)/flex.configure: $(BUILD_DIR)/bison.install
	$(prepare)
	$(call configure, YACC=$(INSTALL_PREFIX)/bin/yacc)

# For debugging this makefile
echo:
	@echo $($(V))
